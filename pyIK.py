#!/usr/bin/env python
# -*- coding: utf-8 -*-

from numpy import *

_INDEX_X = 0
_INDEX_Y = 1
_INDEX_Z = 2


def _get_position(angle, length):
    return array([cos(angle) * length, sin(angle) * length, 0])


def _get_distance(pos1, pos2):
    sum_sqr = 0
    for i, j in zip(pos1, pos2):
        sum_sqr += (int(i) - int(j)) ** 2
    return sqrt(sum_sqr)


def _get_angle(pos1, pos2):
    return arctan2(pos2[_INDEX_Y] - pos1[_INDEX_Y], pos2[_INDEX_X] - pos1[_INDEX_X])


class IK:
    # углы в радианах
    angles = None
    # длины плеч
    length = None
    positions = None

    def __init__(self, length, angles):
        self.angles = angles
        self.length = length

        self.positions = zeros(shape=[1, 3])
        for i in range(0, len(self.angles)):
            pos = array(_get_position(self.angles[i], self.length[i]))
            pos = [pos]
            self.positions = append(self.positions, pos, axis=0)
            self.positions[i + 1] += self.positions[i]

    def calc_angles(self, pos_target, min_err=0.1, max_iter=10):
        iter_num = 0
        while (_get_distance(self.positions[-1], pos_target) > min_err) and ((iter_num < max_iter) or (max_iter == 0)):
            # forward
            self.positions[-1] = pos_target
            for i in range(len(self.positions) - 2, -1, -1):
                self.angles[i] = _get_angle(self.positions[i], self.positions[i + 1])
                self.positions[i] = self.positions[i + 1] - _get_position(self.angles[i], self.length[i])

            # back
            self.positions[0] = [0, 0, 0]
            for i in range(1, len(self.positions)):
                self.angles[i - 1] = _get_angle(self.positions[i - 1], self.positions[i])
                self.positions[i] = self.positions[i - 1] + _get_position(self.angles[i - 1], self.length[i - 1])

            iter_num += 1
