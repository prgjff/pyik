#!/usr/bin/env python
# -*- coding: utf-8 -*-

from numpy import *
import pyIK
import pygame
from pygame import *
import math

set_printoptions(precision=2, suppress=True)

length = array([100.0, 100.0])
# углы в радианах
angles = array([1.57, 1.57])
target_pos = array([150, 100, 0])

ik = pyIK.IK(length=length, angles=angles)
ik.calc_angles(target_pos)


def set_servo(servo_channel, position):
    servo_str ="%u=%u\n" % (servo_channel, position)
    with open("/dev/servoblaster", "wb") as f:
        f.write(servo_str)


def arm_update():
    set_servo(1, math.degrees(ik.angles[0]))
    set_servo(2, math.degrees(ik.angles[1]))

print (ik.positions)
print (ik.positions[:, (0, 1)])
print (ik.angles)

WIN_WIDTH = 600
WIN_HEIGHT = 600
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
BACKGROUND_COLOR = "#000000"
lines = zeros_like(ik.positions[:, (0, 1)])
timer = pygame.time.Clock()

def main():
    pygame.init()
    screen = pygame.display.set_mode(DISPLAY)
    pygame.display.set_caption("Super Mario Boy")
    bg = Surface((WIN_WIDTH, WIN_HEIGHT))

    bg.fill(Color(BACKGROUND_COLOR))

    while True:
        timer.tick(100)
        for e in pygame.event.get():
            if e.type == QUIT:
                raise SystemExit, "QUIT"
            if e.type == pygame.MOUSEMOTION:
                pos = pygame.mouse.get_pos()
                target_pos[0] = pos[0] - WIN_WIDTH / 2
                target_pos[1] = pos[1] - WIN_HEIGHT / 2
                ik.calc_angles(target_pos)
                arm_update()
                print (str(math.degrees(ik.angles[0])) + " " + str(math.degrees(ik.angles[1])))

        screen.blit(bg, (0,0))

        lines[:, 0] = ik.positions[:, 0] + WIN_WIDTH / 2
        lines[:, 1] = ik.positions[:, 1] + WIN_HEIGHT / 2
        pygame.draw.lines(screen, (255, 0, 0), False, lines, 8)
        pygame.display.update()

if __name__ == "__main__":
    main()